---
layout: page
title: "Ciclo de Charlas Informáticas (CCI)"
date: 2013-01-24 23:28
comments: false
sharing: true
footer: true
---

{% img left http://new.csrg.inf.utfsm.cl/wp-content/uploads/2011/04/cci_1.png %}
Nuestro Ciclo de Charlas Informáticas (CCI) es una instancia creada por el Departamento de Informática de la Universidad Técnica Federico Santa María, en conjunto con los estudiantes de CSRG (Computer Systems Research Group) para compartir los conocimientos adquiridos por el grupo en temas como: Administración de Sistemas, Programación, Desarrollo de Proyectos, Investigación en nuevas Herramientas de Software, entre otros. Además para dar a conocer las últimas novedades en tecnologías existentes en el mercado como así también las implantadas en los Laboratorios de nuestro Departamento.


Enlace web: [http://cci.inf.utfsm.cl](http://cci.inf.utfsm.cl)

## Historia

CCI nace como la unión de esfuerzos de Charlas Técnicas y Coloquio del Departamento de Informática de la UTFSM. Hace ya varios años en el Laboratorio de Computación, LabComp, empezó una actividad en la que los alumnos compartían sus conocimientos en charlas informales, con el tiempo éstas fueron tomando forma, abarcando temas de interés más general, un público más masivo y una presencia a nivel regional. Del 2006 al 2008, el ciclo de Charlas Técnicas llegó a convertirse en un ciclo de charlas semanales, con invitados externos de la U, además de profesores y alumnos del mismo Departamento de Informática. El grupo de alumnos recibió apoyo directo del Departamento y el profesor Dr. Horst von Brand fue el primer profesor a cargo de Charlas Técnicas. Por otra parte, existía el Coloquio de Informática organizado por los profesores del Departamento donde se invitaba a especialistas a conversar sobre la disciplina misma. En el año 2009 se unieron ambas iniciativas, la Profesora Cecilia Reyes asume la coordinación, se crea CCI como parte de CSRG, y es así como hoy se cuenta con un gran equipo compuesto por alumnos y profesores de diferentes áreas de la informática que han permitido convertir a CCI en un ciclo semanal abierto a la comunidad con charlas de calidad en variados temas de informática.


## Actividades, Charlas 

Las actividades nuestras son semana a semana, los días jueves a las 12hrs (con algunas excepciones), se puede tener un detalle de las charlas próximas en el siguiente enlace:  [Calendario Ciclo Charlas Informáticas](http://cci.inf.utfsm.cl/?page_id=33)

Así también se va haciendo registro de todas las charlas que se realizan, para así tener la disponibilidad de estas en cualquier momento, eso se puede ver en los Ciclos Anteriores (trabajo que está en proceso en este minuto) en el siguiente enlace: [Ciclos anteriores](http://cci.inf.utfsm.cl/?page_id=36)
