---
layout: page
title: "Acerca de CSRG"
date: 2013-01-24 14:56
comments: false
sharing: true
footer: true
---

UTFSM Computer Systems Research Group (CSRG) es un grupo de investigación que
agrupa a diversos proyectos del área de sistemas computacionales. Bajo el
paradigma del “aprender haciendo”, se trabaja mano a mano con ingenieros y
científicos de clase mundial, produciendo desarrollos de altos estándares de
calidad y de utilidad para la comunidad.


### Misión

Aprender acerca de sistemas computacionales reales, complejos y distribuidos,
trabajando lado a lado con proyectos de clase mundial, obteniendo conocimiento
invaluable acerca de tecnologías de software. Comprender los puntos de vista
teóricos y prácticos, uniendo conocimiento académico con experiencias reales de
ingenieros y científicos al rededor del mundo. Haciendo todo este conocimiento
útil para toda la humanidad. Nuestra misión en resumen es “aprender haciendo”.


### Visión

Nuestra visión es trabajar lado a lado con los mejores ingenieros y científicos,
desarrollando proyectos transcendentales utilizando los últimos estándares y
tecnologías, de tal forma de que los productos de nuestro trabajo sean
utilizados y mantenidos por mucho tiempo, siendo reconocidos en el mundo como un
ejemplo de trabajo y colaboración de alta calidad.
  
  
  
  
### Quienes Somos


Estamos conformados por estudiantes y profesores que lideran y desarrollan
diferentes iniciativas académicas dentro del Departamento de Informática de la
UTFSM.



### Objetivos

  * Incentivar el desarrollo de proyectos de investigación orientados a la generación de nuevos conocimientos y  tecnología.
  * Compartir y difundir los conocimientos generados con toda la comunidad.


### Metodología

Como organización conformada por diferentes proyectos, compartimos los
diferentes recursos que poseemos entre las iniciativas, buscando apoyar y
ayudarse mutuamente entre sí. La organización de cada proyecto la definen cada
uno de sus integrantes, mientras que CSRG es dirigido por miembros de cada uno
de los proyectos participantes en él.


### Nuestra Historia


En Junio del 2004, tres estudiantes de Ingeniería Civil Informática de la UTFSM
formaron el ACS Team. Un número de proyectos con colaboradores internacionales
fueron iniciados, especialmente con el European Southern Observatory (ESO). En
Junio del 2006, CSRG fue formalmente creado y desde entonces la participación
estudiantil en éste tipo de proyectos a ha aumentado notoriamente, originando
fuertes colaboraciones con organizaciones de renombre internacional.

CSRG es reconocido como parte del programa PIE>A desde el 2008, y actualmente es
conformado por más de 30 estudiantes y profesores de la universidad.The UTFSM
CSRG is a research group which gathers together various projects in the computer
systems area. The main idea behind our work is to "learn and do", collaborating
with world class engineers and scientists, and producing high quality
development, useful for the community.
