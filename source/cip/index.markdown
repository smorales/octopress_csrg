---
layout: page
title: "Campeonato Interescolar de Programación (CIP))"
date: 2013-01-24 23:31
comments: true
sharing: true
footer: true
---

{% img left http://csrg.inf.utfsm.cl/wp-content/uploads/2011/06/logo_final_cip_csrg_prev.png) %}
El Campeonato Interescolar de Programación (CIP) es una iniciativa orientada a estudiantes de Enseñanza Media que busca potenciar el uso del computador como herramienta para resolver problemas. Los principales objetivos del CIP son:


  * Contribuir a una mejor educación de los jóvenes secundarios.
  * Estimular la creatividad y capacidad para resolver problemas usando el ingenio y la lógica.
  * Promover actividades que demanden mayor interés, preparación y superación en conocimientos sobre Informática.
  * Destacar a los jóvenes que demuestren talento y aptitudes en el campo de la informática y apoyarlos en su futura formación.


## Actividades

  * Talleres:  Clases presenciales orientadas a que los estudiantes secundarios conozcan el mundo de la programación, aprendan a crear sus propios programas y se acerquen al entorno universitario (programación es un ramo común en las mallas de Ingeniería).
  * Competencia: Instancia para que los estudiantes puedan demostrar sus habilidades para programar a través de la resolución de problemas. Consiste en una sesión de 3 horas, en la cual se compite individualmente por resolver una cantidad determinada de problemas en el menor tiempo posible.


## Versiones

  * [Talleres de Programación 2010](https://www.facebook.com/media/set/?set=a.175595472464197.41356.164850630205348&type=3)
  * [Talleres de Programación 2011](https://www.facebook.com/media/set/?set=a.263978786959198.73723.164850630205348&type=3)
  * [Competencia 2011](https://www.facebook.com/media/set/?set=a.292306254126451.80537.164850630205348&type=3)


## Enlaces
   
  * ![web](https://csrg.inf.utfsm.cl/twiki4/pub/TWiki/TWikiDocGraphics/globe.gif)[ Sitio web](http://cip.usm.cl)
  * ![facebook](https://csrg.inf.utfsm.cl/twiki4/pub/Main/IsraelLeiva/icono_facebook.gif)[ Facebook](http://www.facebook.com/talleres.usm)
  * ![twitter](https://csrg.inf.utfsm.cl/twiki4/pub/TWiki/TWikiDocGraphics/twitter.gif)[ Twitter](http://www.twitter.com/cip_usm))])])])])])])])])])))]
