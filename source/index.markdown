---
layout: frontpage
sidebar: collapse
---

Bienvenidos al portal de CSRG-UTFSM. CSRG agrupa al conjunto de proyectos desarrollados por alumnos y profesores del Departamento de Informática
de la UTFSM orientados a la investigación y/o generación de nuevos conocimientos y tecnología.
