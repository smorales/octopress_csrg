---
layout: page
title: "ALMA UTFSM"
date: 2013-01-24 15:46
comments: false
sharing: true
footer: true
---


![ALMA-UTFSM](http://twiki.csrg.cl/twiki/pub/ACS/WebHome/logo_alma_utfsm.jpg)

El grupo ALMA-UTFSM fue creado como un equipo de investigación en sistemas
distribuídos en aplicaciones astronómicas y no astronómicas en el año 2004.


La elección de la plataforma de desarrollo [ALMA Common Software
framework](http://www.eso.org/~almamgr/AlmaAcs/index.html) (ACS) proviene de la
experiencia adquirida durante algunas prácticas realizadas en observatorios
[ESO](http://www.eso.org/). Despues de tres años de contribuciones informales
hacia el desarrollo de ACS, el equipo presentó una iniciatica de intercambio
tecnológico a los [fondos
ALMA-CONICYT](http://ri.conicyt.cl/575/article-11713.html) 2006, los cuales
fueron obtenidos en el 2007.

A través de los años previos, la UTFSM ha ayudado al equipo de ACS con
aplicaciones _nice-to-have_ y pruebas de software, pero sin duda el gran apoyo
al desarrollo ha sido en el último par de años, en los cuales nos hemos
integrado al desarrollo de tecnologías más complejas, como tópicos en
Inteligencia Articial, Generación de código, etc.

Hoy en día, ALMA-UTFSM es parte del [Computer Systems Research
Group](http://twiki.csrg.cl/twiki/pub/ACS/WebHome/logo_alma_utfsm.jpg) y está
involucrado en variadas contribuciones a ACS.

Otro punto importante es el desarrollo de iniciativas de tranferencia
tecnológica. Variadas [colaboraciones
inter-universitarias](https://csrg.inf.utfsm.cl/twiki4/bin/view/ACS/AcsCollaboration)
han aflorado luego del primer [ACS Workshop](http://acsworkshop.inf.utfsm.cl/)
organizado y realizado en la UTFSM en el año 2007. Hoy, más de seis miembros del
equipo están trabajando como parte del grupo de computación de ALMA. Otros siete
estudiantes and trabajado durante sus prácticas de verano en desarrollos
relacionados con ALMA. Actualmente, [cerca de 15
personas](https://csrg.inf.utfsm.cl/twiki4/bin/view/ACS/AcsStaff) están
involucrada en las actividades del grupo, tanto en Valparaíso (Casa Central)
como en Santiago (Capus San Joaquin).


## Colaboraciones

  * Fondos
  * Instituciones
  * Organizaciones


## Actividades

### Generic State Machine Engine Project

MDA (Model Driven Architecture) approach for software development is highly
appraised when the project in hand has a big size, such is the case of
ALMA. This approach allows to maintain a vertebral spine in the communication of
the development; from the very first design, down to the code finally delivered,
and even beyond, automating the deployment. This allows the maximization of
reusable models, and enhances the propagation of the architecture and design of
the project to other levels in the software development pipeline. Up to this
moment, there is a throughout use of models in the development of ALMA, but only
in the upper most stages of software development. Code generation allows to use
models to quickly create re-usable transformations that translates models to
working code. The main focus for this research area will be state machines code
generation, allowing the quick creation of functional ALMA applications, that
has a specified behavior, all from a simple graphic model.


### ACS Windows Porting


This project has a main idea the exploration of ACS porting to Microsoft Windows
and its maintenance in the future, the objectives are, to have a functional Java
ACS in windows, then, a functional C++ porting and finally, ACS should be
functional in Python.


### Remote Python Course


The objective of the course for the students is to be able to program in Python
in order to use effectively the object-oriented interface of ALMA devices. The
target audience is: electronic engineers, mechanical engineers, telescope
operators.


### ALMA Lego Simulaton


The Alma Lego Simulator is a project intended to spread information about ALMA
and the functioning of the antennas and transporters. This will be achieved with
the building of two Lego models: an antenna and a transporter truck (more models
can be added), and this models will be fully functional with motors, and
controlled with a control software running over the distributed framework Alma
Common Software (ACS).  This project has a strong emphasis in the software
development, because the simulation of the interaction between the antenna and
the transporter is very complex. The simulation has to be able to have a
coordinated control, where the software controls simultaneously the two models,
simulating the observation by providing a coordinated movement of the antennas
and the rearrangement of the ALMA antennas by rising and moving an antenna with
the transporter truck.


## Publicaciones

  * 2010
    * _A methodological proposal for the development of an HPC-based antenna
      array scheduler_, Roberto Bonvallet, Arturo Hoffstadt, Diego Herrera,
      Daniela Lopez, Rodrigo Gregorio, Manuel Almuna, Rafael Hiriart y Mauricio
      Solar, SPIE 2010

    * _High performance graphical data trending in a distributed system_,
      Cristian Maureira, Arturo Hoffstadt, Joao Lopez, Rodrigo Tobar, Nicolas
      Troncoso y Horst von Brand, SPIE 2010
	
    * _New architectures support for ALMA Common Software: Lessons learned and
      taught_, Camilo Menay, Gabriel Zamora, Rodrigo Tobar, Jorge Avarias, Kevin
      Dahl-Skog, Horst von Brand y Gianluca Chiozzi, SPIE 2010

    * _Integrating a university team in the ALMA software development process: a
      successful model for distributed collaborations_, Matias Mora, Jorge
      Ibsen, Gianluca Chiozzi, Nicolas Troncoso, Rodrigo Tobar, Mauricio Araya,
      Jorge Avarias y Arturo Hoffstadt, SPIE 2010

    * _A code generation framework for ALMA common software_, Nicolas Troncoso,
      Horst von Brand, Jorge Ibsen, Matias Mora, Victor Gonzalez, Gianluca
      Chiozzi, Bogdan Jeram, Heiko Sommer, Gabriel Zamora y Alexis Tejeda, SPIE
      2010

    * _A survey on the dynamic scheduling problem in astronomical observations_,
      Matias Mora, Mauricio Solar, WCC/IFIP-AI 2010


  * 2009
    * _A Reference Architecture Specification of a Generic Telescope Control
      System_, Joao Lopez, Rodrigo Tobar, Tomas Staig, Mauricio Araya, Camilo
      Menay, Daniel Bustamante, Horst von Brand, ADASS 2009
	
    * _Reusable state machine code generator_, Arturo Hoffstadt, Heiko Sommer,
      Luigi Andolfato, Cecilia Reyes, ADASS 2009

    * _Adding support to ACS for Real-Time operations through the usage of a
      POSIX-compliant RTOS_, Rodrigo Tobar, Mauricio Araya, Thomas Juerges,
      Horst von Brand, ADASS 2009

    * _Extending the device support for the ALMA Control subsystem code
      generation framework_, Johnny Reveco, Matias Mora, Tomas Staig, Jorge
      Ibsen, Jeff Kern, Víctor González, Norman Saez, Thomas Juerges, Cecilia
      Reyes, ADASS 2009

    * _ALMA Common Software(ACS), Status and Development_, Gianluca Chiozzi,
      Bogdan Jeram, Alessandro Caproni, Heiko Sommer, Matej Sekoranja, Joseph
      Schwarz, Roberto Cirami, Hiroshi Yatagai, Jorge Avarias, Arturo Hoffstadt,
      Joao Lopez, Arne Grimstrup, ICALEPCS 2009
	
    * _Porting a large distributed control framework to the Windows Platform_,
      Rodrigo Tobar, Camilo Menay, Jorge Avarias, Horst von Brand, WSDP2009
	
    * _ALMA Common Software: An open-souce platform for distributed control_,
      Camilo Menay, Arturo Hoffstadt, EL2009

	
  * 2008

    * _An Amateur Telescope Control System: Towards a Generic Telescope Control
      Model_, Rodrigo Tobar, Horst von Brand, Mauricio Araya y Joao Lopez, SPIE
      2008
	
    * _Una Aplicación Astronómica enfocada a la Educación a través de una
      Plataforma de Control de Telescopios Distribuida_, Carlos Guajardo, Tomas
      Staig, Horst von Brand, JAIIO 2008

    * _Towards a Distributed Control Framework with Real-Time Support_, Mauricio
      Araya, Rodrigo Tobar, Jorge Avarias, Horst von Brand, CLEI 2008

    * _Hardware device simulation framework in the ALMA Control subsystem_,
      Matias Mora, Jorge Ibsen, Jeff Kern, Rodrigo Araya, Nicolas Troncoso,
      Victor Gonzalez, Ralph Marson, Thomas Juerges, Allen Farris y
      CeciliaReyes, ADASS 2008

    * _A Reference Implementation of a Generic Automatic Pointing System_, Tomas
      Staig, Rodrigo Tobar, Mauricio Araya, Carlos Guajardo y Horst von Brand,
      ADASS 2008

    * _CSAT Deployment and Final Status_, Rodrigo Tobar, Joao Lopez, Mauricio
      Araya, Horst von Brand, ECC08

    * _ACS Workshop -- An Experience Teaching a Complex Framework_, Horst von
      Brand, Mauricio Araya, Jorge Ibsen, CCESC08

  * 2007
	
    * _ALMA Common Software - UTFSM Group_, Mauricio Araya, Jorge Avarias,
      Matias Mora y Rodrigo Tobar, SOCHIAS

    * _Software Development for ALMA in Chile: The ACS-UTFSM Group_, Horst von
      Brand, SOCHIAS
